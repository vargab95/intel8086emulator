#include "disassembler.h"

disassembler_return_code_t disassemble_binary(instruction_vector_t *instruction_vector, FILE *fp)
{
    char buffer[128];
    instruction_return_code_t instruction_return_code = INSTRUCTION_OK;

    for (size_t i = 0; i < instruction_vector->length && instruction_return_code == INSTRUCTION_OK; i++)
    {
        instruction_return_code = instruction_disassemble(instruction_vector->instructions[i], 
                                                          buffer, 
                                                          sizeof(buffer) / sizeof(buffer[0]));

#ifdef DEBUG_INSTRUCTION_DISASSEMBLY
        instruction_print(instruction_vector->instructions[i], fp);
#endif

        fprintf(fp, "%s\n", buffer);
    }

    return instruction_return_code == INSTRUCTION_OK ? DISASSEMBLER_OK : DISASSEMBLER_PARSING_ERROR;
}

disassembler_return_code_t disassemble_binary_file(FILE *binary, FILE *fp)
{
    instruction_parse_result_t parse_result = instruction_parse_binary(binary);
    if (parse_result.return_code != INSTRUCTION_OK)
    {
        return DISASSEMBLER_PARSING_ERROR;
    }

    fprintf(fp, "BITS 16\n");

    disassembler_return_code_t return_code = disassemble_binary(parse_result.instruction_vector, fp);

    free(parse_result.instruction_vector);

    return return_code;
}
