#ifndef __DISASSEMBLER_H__
#define __DISASSEMBLER_H__

#include "instruction.h"

typedef enum {
    DISASSEMBLER_OK = 0,
    DISASSEMBLER_DECODING_ERROR,
    DISASSEMBLER_PARSING_ERROR
} disassembler_return_code_t;

extern disassembler_return_code_t disassemble_binary(instruction_vector_t *instruction_vector, FILE *fp);
extern disassembler_return_code_t disassemble_binary_file(FILE *binary, FILE *fp);

#endif
