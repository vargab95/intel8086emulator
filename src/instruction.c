#include "instruction.h"
#include <stdbool.h>

static char reg_name_table[2][8][3] = {
    {"AL", "CL", "DL", "BL", "AH", "CH", "DH", "BH"},
    {"AX", "CX", "DX", "BX", "SP", "BP", "SI", "DI"}
};

static const char *instruction_name_table[] = {
    "1_1", "1_2", "1_3", "1_4", "1_5", "1_6", "1_7", "1_8",
    "2_1", "2_2", "2_3", "2_4", "2_5", "2_6", "2_7", "2_8",
    "3_1", "3_2", "3_3", "3_4", "3_5", "3_6", "3_7", "3_8",
    "4_1", "4_2", "4_3", "4_4", "4_5", "4_6", "4_7", "4_8",
    "5_1", "5_2", "MOV", "5_4", "5_5", "5_6", "5_7", "5_8",
    "6_1", "6_2", "6_3", "6_4", "6_5", "6_6", "6_7", "6_8",
    "7_1", "7_2", "7_3", "7_4", "7_5", "7_6", "7_7", "7_8",
    "8_1", "8_2", "8_3", "8_4", "8_5", "8_6", "8_7", "8_8",
};

static void print_bit_string(FILE *fp, unsigned char *ptr, size_t no_bytes)
{
    for (int i = (no_bytes * 8) - 1; i >= 0; i--)
    {
        const int byte_no = i / 8;
        const int bit_no = i % 8;
        fprintf(fp, ptr[byte_no] & (1 << bit_no) ? "1" : "0");
        if (byte_no != 0 && bit_no == 0)
        {
            fprintf(fp, "_");
        }
    }
}

instruction_return_code_t instruction_disassemble(const instruction_t instruction, char *buffer, size_t buffer_length)
{
    sprintf(buffer, "%s %s, %s",
            instruction_name_table[instruction.s.opcode],
            reg_name_table[instruction.s.word][instruction.s.register_operand_2],
            reg_name_table[instruction.s.word][instruction.s.register_operand_1]);

    return INSTRUCTION_OK;
}

instruction_return_code_t instruction_print(const instruction_t instruction, FILE *fp)
{
    unsigned char binary_print_helper;

    fprintf(fp, "INSTRUCTION: 0x%x (", instruction.i);
    print_bit_string(fp, (unsigned char*)&instruction.i, sizeof(instruction.i));
    fprintf(fp, ")\n");

    binary_print_helper = instruction.s.opcode;
    fprintf(fp, "OPCODE: %d (", instruction.s.opcode);
    print_bit_string(fp, &binary_print_helper, sizeof(binary_print_helper));
    fprintf(fp, ")\n");

    binary_print_helper = instruction.s.direction;
    fprintf(fp, "DIRECTION: %s (", instruction.s.direction ? "Destination in register" : "Source in register");
    print_bit_string(fp, &binary_print_helper, sizeof(binary_print_helper));
    fprintf(fp, ")\n");

    binary_print_helper = instruction.s.word;
    fprintf(fp, "WORD/BYTE: %s (", instruction.s.word ? "WORD" : "BYTE");
    print_bit_string(fp, &binary_print_helper, sizeof(binary_print_helper));
    fprintf(fp, ")\n");

    switch(instruction.s.register_mode)
    {
        case INSTRUCTION_MODE_MEMORY_NO_DISPLACAMENT:
            fprintf(fp, "REGISTER MODE: %s (", "No displacement");
            break;
        case INSTRUCTION_MODE_MEMORY_8_BIT_DISPLACAMENT:
            fprintf(fp, "REGISTER MODE: %s (", "8 bit displacement");
            break;
        case INSTRUCTION_MODE_MEMORY_16_BIT_DISPLACAMENT:
            fprintf(fp, "REGISTER MODE: %s (", "16 bit displacement");
            break;
        case INSTRUCTION_MODE_MEMORY_REGISTER_MODE:
            fprintf(fp, "REGISTER MODE: %s (", "Register mode");
            break;
        default:
            return INSTRUCTION_DECODING_ERROR;
    }

    binary_print_helper = instruction.s.register_mode;
    print_bit_string(fp, &binary_print_helper, sizeof(binary_print_helper));
    fprintf(fp, ")\n");

    binary_print_helper = instruction.s.register_operand_1;
    fprintf(fp, "REG: %d (", instruction.s.register_operand_1);
    print_bit_string(fp, &binary_print_helper, sizeof(binary_print_helper));
    fprintf(fp, ")\n");

    binary_print_helper = instruction.s.register_operand_2;
    fprintf(fp, "R/M: %d (", instruction.s.register_operand_2);
    print_bit_string(fp, &binary_print_helper, sizeof(binary_print_helper));
    fprintf(fp, ")\n");

    return INSTRUCTION_OK;
}

instruction_parse_result_t instruction_parse_binary(FILE *fp)
{
    instruction_vector_t *vector = NULL;
    const size_t instruction_increment = 1024;
    size_t no_instructions_to_allocate = 0;
    bool buffer_is_full = false;

    size_t no_read_shorts = 0;
    do
    {
        no_instructions_to_allocate += instruction_increment;
        vector = realloc(vector, sizeof(vector->length) + no_instructions_to_allocate * sizeof(instruction_t));
        if (NULL == vector)
        {
            return (instruction_parse_result_t){
                .return_code = INSTRUCTION_MEMORY_ERROR,
                .instruction_vector = NULL
            };
        }

        const bool first_read = no_read_shorts == 0;
        if (first_read)
        {
            vector->length = 0;
        }

        no_read_shorts = fread(&vector->instructions[vector->length], 
                               sizeof(uint16_t), 
                               instruction_increment, 
                               fp);

        vector->length += no_read_shorts;
        buffer_is_full = no_read_shorts == instruction_increment;
    } while (buffer_is_full);

    if (no_read_shorts < 0) 
    {
        return (instruction_parse_result_t){
            .return_code = INSTRUCTION_PARSING_ERROR,
            .instruction_vector = NULL
        };
    }

    return (instruction_parse_result_t){
        .return_code = INSTRUCTION_OK,
        .instruction_vector = vector
    };
}
