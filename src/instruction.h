#ifndef __INSTRUCTION_H__
#define __INSTRUCTION_H__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef union {
    uint16_t i;
    struct {
#ifdef BIG_ENDIAN
#define INSTRUCTION_USE_BYTE 0
#define INSTRUCTION_USE_WORD 1
        uint16_t word: 1;

#define INSTRUCTION_SOURCE_IN_REG_1 0
#define INSTRUCTION_DEST_IN_REG_1 1
        uint16_t direction: 1;

        uint16_t opcode: 6;

        uint16_t register_operand_2: 3;
        uint16_t register_operand_1: 3;

#define INSTRUCTION_MODE_MEMORY_NO_DISPLACAMENT 0
#define INSTRUCTION_MODE_MEMORY_8_BIT_DISPLACAMENT 1
#define INSTRUCTION_MODE_MEMORY_16_BIT_DISPLACAMENT 2
#define INSTRUCTION_MODE_MEMORY_REGISTER_MODE 3
        uint16_t register_mode: 2;
#else
#error "Little endian is not supported"
#endif
    } s;
} instruction_t;

typedef struct {
    size_t length;
    instruction_t instructions[];
} instruction_vector_t;

typedef enum {
    INSTRUCTION_OK = 0,
    INSTRUCTION_MEMORY_ERROR,
    INSTRUCTION_DECODING_ERROR,
    INSTRUCTION_PARSING_ERROR
} instruction_return_code_t;

typedef struct {
    instruction_return_code_t return_code;
    instruction_vector_t *instruction_vector;
} instruction_parse_result_t;

extern instruction_return_code_t instruction_disassemble(const instruction_t instruction, char *buffer, size_t buffer_length);
extern instruction_return_code_t instruction_print(const instruction_t instruction, FILE *fp);
extern instruction_parse_result_t instruction_parse_binary(FILE *fp);

#endif
