#include <stdio.h>
#include "disassembler.h"

int main(int argc, char **argv)
{
    return disassemble_binary_file(stdin, stdout);
}
